# zj-vscode-stock 

VScode 插件 实时stocks数据

好好工作，天天发鸿运！！

## 配置
可在文件-首选项-扩展中对一些参数进行配置。
支持配置：涨跌颜色、数据获取频率、股票代码

## 打包发布
```sh
#安装扩展
yarn 

#编译
yarn run package

#打插件包，会生成一个本地安装包
vsce package

#发布插件
vsce publish 
```

## 作者
作者暂时是广东八块钱网的，具体是谁你猜猜
